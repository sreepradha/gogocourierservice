package com.spring.bo;

import java.util.Map;

import com.spring.model.Courier;
import com.spring.model.ServiceChargeInfo;

public class CourierBO  {
	
	public double calculateCourierCharge(Courier cObj,String city) {
	   
		double courierCharge=0.0;
		//fill the code
		ServiceChargeInfo s= cObj.getServiceCharge();
		Map<String,Float> key = s.getLocationServiceCharge();
		 if(key.containsKey(city)){
			 courierCharge = cObj.getWeight()*cObj.getChargePerKg();
			 courierCharge = courierCharge + key.get(city);
		    } 
		  else{
		    	courierCharge = cObj.getWeight()*cObj.getChargePerKg();
		    }
		 return courierCharge;
	}

}
