package com.spring.main;

import java.util.Scanner;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.exception.InvalidParcelWeightException;
import com.spring.model.Courier;
import com.spring.service.CourierService;

public class Driver {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
	   System.out.println("Enter the courier ID:");
	   int id = Integer.parseInt(sc.nextLine());
	   System.out.println("Enter the total weight of parcel:");
	   int weight= Integer.parseInt(sc.nextLine());
	   System.out.println("Enter the city:");
	   String city = sc.nextLine();
	   
	   ApplicationContext ac =new ClassPathXmlApplicationContext("beans.xml");
		CourierService c = (CourierService) ac.getBean("cs",CourierService.class);
	   
		try {
			double charge = c.calculateCourierCharge(id, weight, city);
			System.out.println("Total Courier Charge:"+charge);
		} catch (InvalidParcelWeightException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	}

}
